/* See LICENSE file for copyright and license details. */

/* appearance */
static unsigned int borderpx  = 2;        /* border pixel of windows */
static int startwithgaps	  = 1;	 /* 1 means gaps are used by default */
static unsigned int gappx     = 3;       /* default gap between windows in pixels */
static unsigned int snap      = 32;       /* snap pixel */

static unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static unsigned int systrayonleft  = 0;   	/* 0: systray in the right corner, >0: systray on left of status text */
static unsigned int systrayspacing = 5;   /* systray spacing */
static int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/

static int showsystray = 1;     /* 0 means no systray */
static int showbar     = 1;        /* 0 means no bar */
static int topbar      = 1;        /* 0 means bottom bar */
static char font[]     = { "JetBrainsMono Nerd Font:size=10:antialias=true:autohint=true"};
static char font2[]    = { "JoyPixels:size=6:antialias=true:autohint=true"};
static char font3[]    = { "FontAwesome:size=10:antialias=true:autohint=true" };

static const char *fonts[]    = {
    font,font2
};
static char dmenufont[]       = "monospace:size=10";
static char normbgcolor[]     = "#222222";
static char normbordercolor[] = "#444444";
static char normfgcolor[]     = "#bbbbbb";
static char selfgcolor[]      = "#eeeeee";
static char selbordercolor[]  = "#005577";
static char selbgcolor[]      = "#005577";
static char *colors[][3] = {
	/*               fg           bg           border   */
	[SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
	[SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class           instance    title      tags mask     switchtotag    isfloating   monitor */
	// { "Gimp",     NULL,       NULL,         0,             0,             1,           -1 },
	{ "firefox",        NULL,       NULL,         2,            1,            0,          -1 },
	{ "LibreWolf",      NULL,       NULL,         2,            1,            0,          -1 },
	{ "Brave-browser",  NULL,       NULL,         2,            1,            0,          -1 },
	{ "qutebrowser",    NULL,       NULL,         2,            1,            0,          -1 },
	{ "Chromium",       NULL,       NULL,         2,            1,            0,          -1 },
	{ "waterfox-current",NULL,      NULL,         2,            1,            0,          -1 },

	{ "Steam",          NULL,       NULL,         3,            0,            1,          -1 },
	{ "Lutris",         NULL,       NULL,         3,            0,            1,          -1 },
	{ "Timeshift-gtk",  NULL,       NULL,         3,            1,            1,          -1 },

	{ "Evolution",      NULL,       NULL,         4,            1,            0,          -1 },
	{ "mpv",            NULL,       NULL,         4,            1,            0,          -1 },
	{ "vlc",            NULL,       NULL,         4,            1,            0,          -1 },
	{ "parole",         NULL,       NULL,         4,            1,            0,          -1 },
	{ "smplayer",       NULL,       NULL,         4,            1,            0,          -1 },

	{ "VirtualBox Manager",NULL,    NULL,         6,            1,            1,          -1 },

    // Floating Rules
    {"Arandr",          NULL,       NULL,         0,            0,            1,          -1 },
    {"Arcologout.py",   NULL,       NULL,         0,            0,            1,          -1 },
    {"albert",          NULL,       NULL,         0,            0,            1,          -1 },
    {"feh",             NULL,       NULL,         0,            0,            1,          -1 },
    {"Galculator",      NULL,       NULL,         0,            0,            1,          -1 },
    {"Nitrogen",        NULL,       NULL,         0,            0,            1,          -1 },
    {"Grub-customizer", NULL,       NULL,         0,            0,            1,          -1 },
    {"Pavucontrol",     NULL,       NULL,         0,            0,            1,          -1 },
    {"Minipad",         NULL,       NULL,         0,            0,            1,          -1 },
    {"Connman-gtk",     NULL,       NULL,         0,            0,            1,          -1 },
    {"Gnome-calculator",NULL,       NULL,         0,            0,            1,          -1 },
    {"Arcolinux-tweak-tool.py",NULL,NULL,         0,            0,            1,          -1 },
    {"Evolution-alarm-notify",NULL,NULL,          0,            0,            1,          -1 },

};

/* layout(s) */
static float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "|CM|",     centeredmaster },
	{ ">CM>",     centeredfloatingmaster },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
/*  Header file */
#include <X11/XF86keysym.h>
/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run_history", "-m", dmenumon,"-i" ,NULL };
static const char *termcmd[]  = { "st", NULL };

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "font",            STRING,  &font },
		{ "font2",           STRING,  &font2 },
		{ "font3",           STRING,  &font3 },
		{ "dmenufont",       STRING,  &dmenufont },
		{ "normbgcolor",     STRING,  &normbgcolor },
		{ "normbordercolor", STRING,  &normbordercolor },
		{ "normfgcolor",     STRING,  &normfgcolor },
		{ "selbgcolor",      STRING,  &selbgcolor },
		{ "selbordercolor",  STRING,  &selbordercolor },
		{ "selfgcolor",      STRING,  &selfgcolor },
		{ "borderpx",        INTEGER, &borderpx },
		{ "snap",          	 INTEGER, &snap },
		{ "showbar",         INTEGER, &showbar },
		{ "topbar",          INTEGER, &topbar },
		{ "nmaster",         INTEGER, &nmaster },
		{ "resizehints",     INTEGER, &resizehints },
		{ "systrayspacing ", INTEGER, &systrayspacing },
		{ "startwithgaps",   INTEGER, &startwithgaps },
		{ "gappx",      	 	 INTEGER, &gappx },
		{ "mfact",      	 	 FLOAT,   &mfact },

};

static Key keys[] = {
	/* modifier                     key                function        argument */
	{ MODKEY,                       XK_d,              spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_d,              spawn,          SHCMD("rofi -show run") },
	{ MODKEY,                       XK_Return,         spawn,          {.v = termcmd } },
	{ MODKEY|ShiftMask,             XK_Return,         spawn,          SHCMD("kitty") },
	{ MODKEY,                       XK_KP_End,         spawn,          SHCMD("xterm") },
	{ MODKEY,                       XK_KP_Enter,       spawn,          SHCMD("alacritty") },

	{ MODKEY,                       XK_e,              spawn,          SHCMD("pcmanfm") },
	{ MODKEY|ShiftMask,             XK_e,              spawn,          SHCMD("st -e vifmrun") },
	{ MODKEY,                       XK_w,              spawn,          SHCMD("qutebrowser") },
	{ MODKEY|ShiftMask,             XK_w,              spawn,          SHCMD("firefox") },
	{ MODKEY,                       XK_g,              spawn,          SHCMD("galculator") },
	{ MODKEY,                       XK_r,              spawn,          SHCMD("rofi -show drun -show-icons") },
	{ MODKEY,                       XK_v,              spawn,          SHCMD("virtualbox") },
	{ MODKEY,                       XK_p,              spawn,          SHCMD("superp") },
	{ MODKEY,                       XK_y,              spawn,          SHCMD("clipboard") },

	{ MODKEY|ControlMask,           XK_s,              spawn,          SHCMD("poweroff 'Wanna Shutdown?' 'poweroff'") },
	{ MODKEY|Mod1Mask,              XK_c,              spawn,          SHCMD("open-rcs") },
	{ MODKEY|Mod1Mask,              XK_g,              spawn,          SHCMD("open-games") },

	{ ControlMask|Mod1Mask,         XK_p,              spawn,          SHCMD("get-class-name") },
	{ ControlMask|Mod1Mask,         XK_c,              spawn,          SHCMD("xcolor | xclip -r -i -sel clip") },
	{ ControlMask|Mod1Mask,         XK_v,              spawn,          SHCMD("pavucontrol") },
	{ ControlMask|Mod1Mask,         XK_e,              spawn,          SHCMD("rofie") },

	{ 0,                            XK_Print,          spawn,          SHCMD("take_ss full") },
	{ ShiftMask,                    XK_Print,          spawn,          SHCMD("take_ss focus") },

	{ MODKEY,                       XK_b,              togglebar,      {0} },

	{ MODKEY,                       XK_j,              focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,              focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_Left,           focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Right,          focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,              rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,              rotatestack,    {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Left,           rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Right,          rotatestack,    {.i = -1 } },

	{ MODKEY,                       XK_h,              setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,              setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Down,           setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_Up,             setmfact,       {.f = +0.05} },

    { MODKEY,                       XK_i,              incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_i,              incnmaster,     {.i = -1 } },

	{ MODKEY,                       XK_bracketleft,    viewtoleft,     {0} },
	{ MODKEY,                       XK_bracketright,   viewtoright,    {0} },
	{ MODKEY|ShiftMask,             XK_bracketleft,    tagtoleft,      {0} },
	{ MODKEY|ShiftMask,             XK_bracketright,   tagtoright,     {0} },
	{ MODKEY,                       XK_Tab,            view,           {0} },
	/* { MODKEY,                       XK_Return,         zoom,           {0} }, */

	{ MODKEY,                       XK_q,              killclient,     {0} },

	{ MODKEY,                       XK_t,              setlayout,      {.v = &layouts[0]} },
    { MODKEY,                       XK_u,              setlayout,      {.v = &layouts[3]} },
    { MODKEY|ShiftMask,             XK_u,              setlayout,      {.v = &layouts[4]} },
	{ MODKEY,                       XK_m,              setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_s,              togglefloating, {0} },
	{ MODKEY,                       XK_f,              togglefullscr,  {0} },

	{ MODKEY,                       XK_0,              view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,              tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,          focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,         focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,          tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,         tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,          setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_equal,          setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_minus,          setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_equal,          setgaps,        {.i = GAP_TOGGLE} },
	TAGKEYS(                        XK_1,                               0)
	TAGKEYS(                        XK_2,                               1)
	TAGKEYS(                        XK_3,                               2)
	TAGKEYS(                        XK_4,                               3)
	TAGKEYS(                        XK_5,                               4)
	TAGKEYS(                        XK_6,                               5)
	TAGKEYS(                        XK_7,                               6)
	TAGKEYS(                        XK_8,                               7)
	TAGKEYS(                        XK_9,                               8)
	{ MODKEY|ControlMask,           XK_x,              quit,           {0} }, // 0 Quit out of dwm
    { MODKEY|ControlMask,           XK_r,              quit,           {1} }, // 1 means restart
    { MODKEY,                       XK_Escape,         quit,           {1} }, // 1 means restart

	{ 0,             XF86XK_AudioRaiseVolume,  spawn,          SHCMD("dwm_vol up; kill -45 $(pidof dwmblocks)") },
	{ 0,             XF86XK_AudioLowerVolume,  spawn,          SHCMD("dwm_vol down; kill -45 $(pidof dwmblocks)") },
	{ 0,             XF86XK_AudioMute,         spawn,          SHCMD("dwm_vol mute; kill -45 $(pidof dwmblocks)") },
	{ 0,             XF86XK_MonBrightnessUp,   spawn,          SHCMD("dwm_light up; kill -44 $(pidof dwmblocks)") },
	{ 0,             XF86XK_MonBrightnessDown, spawn,          SHCMD("dwm_light down; kill -44 $(pidof dwmblocks)") },
	{ ShiftMask,     XF86XK_MonBrightnessUp,   spawn,          SHCMD("dwm_light sup; kill -44 $(pidof dwmblocks)") },
	{ ShiftMask,     XF86XK_MonBrightnessDown, spawn,          SHCMD("dwm_light sdown; kill -44 $(pidof dwmblocks)") },

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
