// Just add 34 to your typical signal number.
// kill -44 $(pidof dwmblocks)
//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "date '+%b %d (%a) %I:%M%p'",  60,		        0},

	{"",        "dwm_mem",	            5,		        0},
	{"",        "dwm_light show",	    0,		        10},
	{"",        "dwm_vol show",	        0,		        11},
	{"",        "dwm_bat",	            5,		        0},
	/* {"",        "dwm_net",	            5,		        0}, */
	{"",        "net_traffic",	        5,		        0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
/* static char delim[] = " "; */
static char delim[] = " ";
static unsigned int delimLen = 8;
