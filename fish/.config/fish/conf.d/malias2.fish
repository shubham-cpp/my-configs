alias df="df -h"
alias du="du -h"
alias free="free -h"

alias curl="curl -O -L -C -"
alias grep="grep -i --color=auto"

alias rg="rg -i"
alias makeblink="echo -e -n \"\x1b[\x30 q\""

# Changing "ls" to "exa"
alias ls="exa -l --color=auto --group-directories-first --icons"
alias ll="exa -al --color=auto --group-directories-first --icons"
alias la="exa -a --color=auto --group-directories-first --icons"
alias lt="exa -aT --color=auto --group-directories-first --icons"
alias lr="exa -aGR --color=auto"

alias rm="trash-put"
alias rmd="trash-put -rf"
alias tls="trash-list"
alias xcp="xclip -i -r -sel clip"
alias xps="xclip -o -r -sel clip"
alias zrc="$EDITOR $HOME/.config/zsh/alias.zsh"
alias szrc="source $HOME/.config/zsh/.zshrc"

alias yay="paru"
alias yays="paru -S --noredownload --needed"
alias yayr="paru -Rncs"
# alias xin="sudo xbps-install -S"
# alias xr="sudo xbps-remove -R"
# alias xs="xbps-query -R --regex -s"

alias ..="cd .."
alias ...="cd ../.."
alias merge="xrdb ~/.config/X11/Xresources"
alias v="nvim"
alias se="sudoedit"
alias mci="make -j5 && sudo make install clean"
alias mdd="sudo dd bs=1M status=progress"
alias sel_font="fc-list | fzf | cut -d':' -f2 | cut -c 2- | xclip -i -r -sel clip"

#get fastest mirrors in your neighborhood
# alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
# alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
# alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
# alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

#Cleanup orphaned packages
# alias cleanup="sudo pacman -Rns (pacman -Qtdq)"
#get the error messages from journalctl
# alias jctl="journalctl -p 3 -xb"

# For Data science,anaconda and jupyter
# alias conda_activate="source /opt/anaconda/bin/activate root"
# alias conda_deactivate="source /opt/anaconda/bin/deactivate root"

# jupyter notebook Start
alias jnl="jupyter notebook list | sed '/running servers/d'"
#alias jnb="(nohup jupyter notebook &> /dev/null &); sleep 2; jnl"
alias jne="ps -ef | awk 'match(\$0,/(jupyter-noteboo|Anaconda\/bin)/)&&!match(\$0,/awk/){print \$2}' | xargs kill -9 2> /dev/null"
#alias jni="wget -qO- https://gitlab.com/MaxdSre/axd-ShellScript/raw/master/assets/software/Anaconda.sh | sudo bash -s -- -C"
alias jnr="sudo /opt/Anaconda/bin/conda remove"
# jupyter notebook End
alias frc="$EDITOR ~/.config/fish/conf.d/malias2.fish"
alias sfrc="source ~/.config/fish/config.fish"
#verify signature for isos
# alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
# alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"
alias gpg-retrieve="gpg --keyserver pool.sks-keyservers.net --recv-keys"

# Coding Related
# Flags for optimizing C/C++

alias gcc="gcc -g -Wall -Wextra -Wfloat-equal -Wundef -Wstrict-prototypes -Wcast-align -Waggregate-return -Wunreachable-code -Wunreachable-code -Wpointer-arith -Wshadow -Wwrite-strings -Wlogical-op -Wmissing-declarations -Wredundant-decls -O3 -foptimize-strlen -ffunction-sections -pipe"

alias g++="g++ -std=c++17 -g -Wall -Wextra -Wfloat-equal -Wundef  -Wcast-align -Waggregate-return -Wunreachable-code -Wunreachable-code -Wpointer-arith -Wshadow -Wwrite-strings -Wlogical-op -Wmissing-declarations -Wredundant-decls -Woverloaded-virtual -Ofast"

# some helpful git aliases
alias g="git"

alias ga="git add"
alias gaa="git add --all"

alias gc="git commit -v"
alias gca="git commit -v -a"
alias gcm="git commit -m"

alias gp="git push"
alias gpu="git push -u origin master"

alias gst="git status"
alias gsb="git status -sb"

alias gupav="git pull --rebase --autostash -v"

alias gcl="git clone"
alias gclr="git clone --recurse-submodules"
