function mgcp -d "Add the Current directory and push it to my-configs.git"
  [ -n "$argv[1]" ]; and git add .; and git commit -m "$argv[1]"; and git push -u origin master; or echo "ERROR : Please pass a message for git commit"
end

function dfs -d "List all my dotfiles and open with neovim"
    rg --hidden --files "$HOME/Documents/dotfiles/" | fzf | xargs -ro -I % $EDITOR % ;or echo "Please provide a directory"
end

function mysc -d "List the Files from bin directory and open with neovim"
    rg --files "$HOME/.local/bin/myscripts/" | fzf | xargs -ro -I % $EDITOR % ;or echo "Please provide a directory"
end

function rcs -d "List the Files from bin directory and open with neovim"
    rg --files "$HOME/.config/nvim/" -g '!tags' -g '!packer_compiled.vim' | fzf | xargs -ro -I % $EDITOR % ;or echo "Please provide a directory"
end
function mkd -d "Create a New directory and cd into it"
    [ -n "$argv[1]" ]; and mkdir -p "$argv[1]"; and cd "$argv[1]"; or echo "Please provide a valid directory name"
end

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
function ex -d "this is a universal function to extract file and not worry about the command"
    set fname $argv[1]
    if test -f $fname
        switch $fname
            case '*.tar.bz2'
                tar xjf $fname
            case '*.tar.gz'
                tar xf $fname
            case '*.bz2'
                bunzip2 $fname
            case '*.rar'
                unrar x $fname
            case '*.gz'
                gunzip $fname
            case '*.tar'
                tar xf $fname
            case '*.tbz2'
                tar xjf $fname
            case '*.tgz'
                tar xzf $fname
            case '*.zip'
                unzip $fname
            case '*.Z'
                uncompress $fname
            case '*.7z'
                7z x $fname
            case '*.deb'
                ar x $fname
            case '*.tar.xz'
                tar xf $fname
            case '*'
                printf "%s cannot be extracted via ex()" $fname
            end
    else
        printf "%s doesn't exists" $fname
    end
end
function run -d "universal function to compile and run programs without worrying about different commands and flags"
    set full_file "$argv[1]"
    set fname (string split -r -m1 . "$argv[1]")[1]
    if test -f $argv[1]
        switch (string split -r -m1 . "$argv[1]")[2]
        case 'c'
            gcc "$full_file" -o "$fname".out; and ./"$fname".out
        case 'cpp'
            g++ "$full_file" -o "$fname".out
; and ./"$fname".out

        case 'java'
            javac "$full_file" ; and java "$fname"
        case 'py'
            python -u "$full_file"
        case 'sh'
            chmod +x "$full_file"; and ./"$full_file"
        case '*'
            printf "%s cannot be executed via run()" $argv[1]
        end

    else
        printf "%s file doesnt exists" $argv[1]
    end
end
    #then
        #case $1 in
            #(*.c) gcc $1 -o $fname && ./$fname ;;
            #(*.cpp) g++ -g $1 -o $fname && ./$fname ;;
            #(*.java) javac $1 && java $fname ;;
            #(*.js) echo "TODO";;
            #(*.sh) chmod +x $1 && ./$1 ;;
            #(*.py) python -u $1 ;;
            #(*) echo "'$1' cannot run with run" ;;
        #esac
    #else
            #echo "'$1' is not a valid file"
    #fi
