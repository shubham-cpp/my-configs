set fish_greeting                      # Supresses fish's intro message
set -a PATH $HOME/.local/bin $HOME/.local/bin/myscripts $HOME/.local/share/npm/bin $HOME/.local/share/golib/bin $HOME/.local/share/luarocks/bin
set -x EDITOR "nvim"
starship init fish | source
