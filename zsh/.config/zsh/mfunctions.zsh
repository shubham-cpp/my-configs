mysc(){
    rg --files "$HOME"/.local/bin/myscripts | fzf --reverse --preview="highlight -O xterm256 --style dante --line-range=1-40 {}" | xargs -ro -I % nvim % || echo "Please provide a directory"
}

dfs(){
    rg --hidden --files "$HOME"/Documents/dotfiles -g '!.git'\
        | fzf --reverse --preview="bat -f -pp --line-range :60 {}"\
        | xargs -ro -I % nvim % || echo "Please provide a directory"
}

rcs(){
    rg --files "$HOME"/.config/nvim -g '!tags' -g '!packer_compiled.vim' -g '!docs' -g '!unused' | fzf --reverse --preview="highlight -O xterm256 --style dante --line-range=1-40 {}" | xargs -ro -I % nvim % || echo "Please provide a directory"
}

vf(){
    fzf --reverse --preview 'bat -f -pp --line-range :60 {}'| xargs -ro -I % nvim %
}

chx(){
    chmod +x "$@"
}

mgcp(){
  [ -n "$1" ] && git add . && git commit -m "$1" && git push -u origin master || echo "ERROR : Please pass a message for git commit"
}

compress(){
    if [ -f $1 ]; then
        case $1 in
            *.pdf) gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$2 $1 ;;
            *) prinf "Cant compress %s file. Seaarch Online if you want" $1
        esac
    else
        prinf "Cant compress %s file. File Doesn't Exists" $1
    fi
}

mkd(){
	mkdir -p $@ && cd ${@:$#}|| echo "Please provide a valid directory name"
    	# mkdir -p "$1" && cd $_
}

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
# ex ()
# {
#   if [ -f $1 ] ; then
#     case $1 in
#       *.tar.bz2)   tar xjf $1   ;;
#       *.tar.gz)    tar xzf $1   ;;
#       *.bz2)       bunzip2 $1   ;;
#       *.rar)       unrar x $1   ;;
#       *.gz)        gunzip $1    ;;
#       *.tar)       tar xf $1    ;;
#       *.tbz2)      tar xjf $1   ;;
#       *.tgz)       tar xzf $1   ;;
#       *.zip)       unzip $1     ;;
#       *.Z)         uncompress $1;;
#       *.7z)        7z x $1      ;;
#       *.deb)       ar x $1      ;;
#       *.tar.xz)    tar xf $1    ;;
#       *)           echo "'$1' cannot be extracted via ex()" ;;
#     esac
#   else
#     echo "'$1' is not a valid file"
#   fi
# }
run () {
    fname=${1%.*}
    if [ -f $1 ]
    then
        case $1 in
            (*.c) gcc $1 -o $fname.out && ./$fname.out ;;
            (*.cpp) g++ -g $1 -o $fname.out && ./$fname.out ;;
            (*.java) javac $1 && java $fname ;;
            (*.js) echo "TODO";;
            (*.sh) chmod +x $1 && ./$1 ;;
            (*.py) python -u $1 ;;
            (*.lua) luajit $1 ;;
            (*) echo "'$1' cannot run with run" ;;
        esac
    else
            echo "'$1' is not a valid file"
    fi
}

#Auto add sudo infront of a command
# sudo-command-line() {
#     [[ -z $BUFFER ]] && zle up-history
#     if [[ $BUFFER == sudo\ * ]]; then
#         LBUFFER="${LBUFFER#sudo }"
#     elif [[ $BUFFER == $EDITOR\ * ]]; then
#         LBUFFER="${LBUFFER#$EDITOR }"
#         LBUFFER="sudoedit $LBUFFER"
#     elif [[ $BUFFER == sudoedit\ * ]]; then
#         LBUFFER="${LBUFFER#sudoedit }"
#         LBUFFER="$EDITOR $LBUFFER"
#     else
#         LBUFFER="sudo $LBUFFER"
#     fi
# }
# zle -N sudo-command-line
# # Defined shortcut keys: [Esc] [Esc]
# bindkey -M emacs '\e\e' sudo-command-line
# bindkey -M vicmd '\e\e' sudo-command-line
# bindkey -M viins '\e\e' sudo-command-line
