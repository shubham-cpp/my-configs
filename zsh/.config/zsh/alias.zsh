alias du="du -h"                                                # Human-readable sizes
# alias wget="wget -c --hsts-file="$XDG_CACHE_HOME/wget-hsts""
alias curl="curl -O -L -C -"
alias grep="grep -i --color=auto"

# Changing "ls" to "exa"
alias ls="exa -l --color=auto --group-directories-first --icons"  # long format
# alias ll="ls -AlhvF --group-directories-first --color=auto"
alias ll="exa -al --color=auto --group-directories-first --icons" # my preferred listing
alias la="exa -a --color=auto --group-directories-first --icons"  # all files and dirs
alias lt="exa -aT --color=auto --group-directories-first --icons" # tree listing
#alias lr='exa -aGR --color=always'
# alias rmd="rm -rf"
alias rm="trash-put"
alias rmd="trash-put -rf"
alias tls="trash-list"
alias zrc="nvim $HOME/.config/zsh/alias.zsh"
alias szrc="source $HOME/.config/zsh/.zshrc"
# alias yays="yay -S --noredownload --needed"
# alias yayr="yay -Rcns"
alias yay="paru"
alias yays="paru -S --noredownload --needed"
alias yayr="paru -Rcns"

alias ..="cd .."
alias ...="cd ../.."
alias v="nvim"
alias se="sudoedit"
alias sel_font="fc-list | fzf | cut -d':' -f2 | cut -c 2- | xclip -i -r -sel clip"

alias xcp="xclip -i -r -sel clip"
alias rg="rg -i"
alias mci="make -j5 && sudo make install clean"

# jupyter notebook Start
alias jnl="jupyter notebook list | sed '/running servers/d'"
alias jnb="(nohup jupyter notebook &> /dev/null &); sleep 2; jnl"
alias jne="ps -ef | awk 'match(\$0,/(jupyter-noteboo|Anaconda\/bin)/)&&!match(\$0,/awk/){print \$2}' | xargs kill -9 2> /dev/null"
# jupyter notebook End

alias gpg-retrieve="gpg --keyserver pool.sks-keyservers.net --recv-keys"

# some helpful git aliases
alias g='git'

alias ga='git add'
alias gaa='git add --all'

alias gc='git commit -v'
alias gca='git commit -v -a'
alias gcm='git commit -m'

alias gp='git push'
alias gpu='git push -u origin master'

alias gst='git status'
alias gsb='git status -sb'

alias gupav='git pull --rebase --autostash -v'

alias gcl='git clone'
