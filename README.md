# Personal Dot files

## Some programs that I use mainly
- WM       :- Awesomewm
- Terminal :- Kitty(main) and xterm(secondary)
- Editor   :- Neovim(nightly)
- Shell    :- Zsh
- Run launcher :- [Dmenu_run_history](https://tools.suckless.org/dmenu/scripts/dmenu_run_with_command_history) and rofi(for superp script)

## Few Scripts that I use often
- [superp](myscripts/.local/bin/myscripts/superp) :- open files from your home directory(Like what CtrlP does for (neo)vim but for Home directory)
