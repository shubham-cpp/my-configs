#!/usr/bin/env bash

PATH="$PATH:$HOME/.local/bin/:$HOME/.local/bin/myscripts/"
PATH="$PATH:$HOME/.local/share/luarocks/bin/:$HOME/.local/share/golib/bin/:$HOME/.local/share/npm/bin/"
PATH="$PATH:$HOME/.local/share/suckless/dwm-62/status-bar/scripts/"
# Deduplicate entries in PATH
typeset -U PATH
# $(du ~/.local/bin/myscripts/ | cut -f2 | paste -sd ':')"
export PATH

export EDITOR=nvim
export VISUAL=nvim

export TERMINAL="st"
export FILE_BROWSER="pcmanfm"
# export BROWSER="firefox"
export BROWSER="qutebrowser"
export READER="zathura"

export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0

# ~/ Clean-up:
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export WINEESYNC=1
export ANDROID_SDK_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/android"
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android
export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android/
export MYSQL_HISTFILE="$XDG_DATA_HOME"/mysql_history
export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android/
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export GOPATH="$XDG_DATA_HOME"/golib
export GOPATH="$GOPATH:$HOME/Documents/Programming/GoLang"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/npm/npmrc"
export NVM_DIR="$XDG_DATA_HOME"/nvm
export ICEAUTHORITY="${XDG_CACHE_HOME:-$HOME/.cache}/ICEauthority"
export RXVT_SOCKET="$XDG_RUNTIME_DIR"/urxvtd
export SCREENRC="$XDG_CONFIG_HOME"/screen/screenrc
export PYLINTHOME="$XDG_CACHE_HOME"/pylint
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME"/jupyter
export JUPYTERLAB_DIR="$XDG_DATA_HOME"/jupyter/lab
export IPYTHONDIR="$XDG_CONFIG_HOME"/jupyter
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export LESSHISTFILE=-
export LESSOPEN="| /usr/bin/highlight -O xterm256 %s 2>/dev/null"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export _ZL_DATA="$HOME/Documents/dotfiles/homefiles/zlua"
export XBPS_DISTDIR="$HOME/Downloads/GitClones/void-packages"
export RANGER_ZLUA="$HOME/Downloads/Programs/Cool-Ones/z.lua/z.lua"
export FZF_DEFAULT_COMMAND="rg --files --hidden --follow --glob '!Git*' --iglob '!void-*' --iglob '!.git*' --iglob '!venv' --iglob '!__pycache__' --iglob '!*.out'"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd --type d --follow"
export FZF_DEFAULT_OPTS="--cycle --marker='+' --keep-right"
# remember --bind KEY:ACTION or EVENT:ACTION
# eval $(dircolors "$XDG_CONFIG_HOME"/dir_colors)
export XCURSOR_PATH=${XCURSOR_PATH}:~/.local/share/icons
# export LUA_PATH="${XDG_DATA_HOME:-$HOME/.local/share}/luarocks/share/lua/5.4/?.lua;;"
# export LUA_CPATH="/usr/lib/lua/5.4/?.so;/usr/lib/lua/5.4/loadall.so;./?.so;$XDG_DATA_HOME/luarocks/lib/lua/5.4/?.so"
export GHCUP_USE_XDG_DIRS="$HOME"/.config
export STACK_ROOT="$XDG_DATA_HOME"/stack
# This is the list for lf icons:
LF_ICONS=$(sed ~/.config/lf/dir_icons \
            -e '/^[ \t]*#/d'       \
            -e '/^[ \t]*$/d'       \
            -e 's/[ \t]\+/=/g'     \
            -e 's/$/ /' | tr '\n' :)
# LF_ICONS=$(echo $LF_ICONS | tr '\n' ':')
export LF_ICONS
