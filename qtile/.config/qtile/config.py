# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from os import getenv,path
# from libqtile.utils import guess_terminal

mod = "mod4"
alt_key = "mod1"
ctrl_key = "control"
# terminal = guess_terminal()
terminal = getenv("TERMINAL", "xterm")
editor = getenv("EDITOR","nano")
browser = getenv("BROWSER","firefox")
home = path.expanduser('~')

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key(
        [mod,alt_key],
        "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        desc="Increase the window towards right",
    ),
    Key(
        [mod,alt_key],
        "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        desc="Increase the window towards right",
    ),
    Key(
        [mod,alt_key],
        "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        desc="Shrink the window towards left",
    ),
    Key(
        [mod,alt_key],
        "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        desc="Shrink the window towards Left",
    ),
    Key(
        [mod,alt_key],
        "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        desc="Increase the window upwards",
    ),
    Key(
        [mod,alt_key],
        "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        desc="Increase the window upwards",
    ),
    Key(
        [mod,alt_key],
        "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        desc="Shrink the window Downwards",
    ),
    Key(
        [mod,alt_key],
        "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        desc="Shrink the window Downwards",
    ),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Apps goes here
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod, "shift"], "Return", lazy.spawn("kitty"),
        desc="Launch Kitty"),
    Key([mod, "shift"], "d",
        lazy.spawn("rofi -show run"),
        desc="Launch Rofi for run commands",
    ),
    Key([mod], "d",
        lazy.spawn("dmenu_run_history"),
        desc="Launch Dmenu",
    ),

    # Toggle between different layouts as defined below
    Key([mod], "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle the Fullscreen Layout"
    ),
    Key([mod], "s",
        lazy.window.toggle_floating(),
        desc="Toggle floating layout"),

    Key([mod],"m",
        lazy.layout.maximize(),
        desc="Toggle minimum and maximum sizes"
    ),
    Key([mod],"n",
        lazy.layout.normalize(),
        desc="Equal all Windows Size"
    ),
    Key([mod],"space",
        lazy.next_layout(),
        desc="Change to next layout"
    ),
    Key([mod],"a",
        lazy.layout.flip(),
        desc="From Left MonadTall to Right MonadTall",
    ),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod], "Escape", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "x", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(),
        desc="Spawn a command using a prompt widget"),

    # Key([mod], "n", lazy.layout.normalize(), desc="Equal all Windows Size"),

    # MULTIMEDIA KEYS
    # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s 10+")),
    Key(["shift"], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s 200")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 10-")),
    Key(["shift"], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 30")),
    # INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("pamixer --toggle-mute ")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer -d 10 --allow-boost ")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 10 --allow-boost ")),
]

# groups = [Group(i) for i in "123456789"]
groups = []
group_names = [str(i) for i in range(1, 10)]
# group_labels = [str(i) for i in range(1, 10)]
group_labels = ["➊", "➋", "➌", "➍", "➎", "➏", "➐", "➑", "➒", "➓"]
# group_labels = ["Ⅰ","Ⅱ","Ⅲ","Ⅳ","Ⅴ","Ⅵ","Ⅶ","Ⅷ","Ⅸ","Ⅹ"]

group_layouts = ["monadtall" for i in range(1, 10)]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        )
    )
for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),

        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),

        Key([mod], "Tab", lazy.screen.toggle_group()),
        # Key([alt_key], "Tab", lazy.screen.next_group()),
        # Key([alt_key, "shift"], "Tab", lazy.screen.prev_group()),
    ])

def monad_layout_theme():
    return {"margin": 2,
            "border_width": 2,
            "border_focus": "#bd93f9",
            "border_normal": "#4c566a",
            "single_border_width": 0,
            "change_size": 10,
            "new_at_current": True,
            }

def init_layout_theme():
    return {
        "margin": 2,
        "border_width": 2,
        "border_focus": "#bd93f9",
        "border_normal": "#4c566a",
    }

monad_settings = monad_layout_theme()
init_theme = init_layout_theme()

layouts = [
    layout.MonadTall(name="Tall", **monad_settings),
    # layout.Columns(border_focus_stack='#d75f5f'),
    layout.Max(),
    layout.Floating(name="Float",**init_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font='RobotoMono Nerd Font',
    fontsize=11,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox("default config", name="default"),
                widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Systray(),
                widget.Clock(format='%a %d %b at %I:%M %p'),
                widget.QuickExit(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
