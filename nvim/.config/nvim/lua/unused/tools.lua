local M = {}
function M.split(s, delimiter)
    --[[ How to use this function
-- local characters = "b s < > [ ]"
-- characters = helper.split(characters," ")
-- for _, value in pairs(characters) do
--     print('='..value)
-- end
-- ]]
    local result = {};
    for match in (s .. delimiter):gmatch("(.-)" .. delimiter) do
        table.insert(result, match);
    end
    return result;
end

function M.is_buf_valid(buf_num)
    if not buf_num or buf_num < 1 then return false end
    local listed = vim.fn.getbufvar(buf_num, "&buflisted") == 1
    local exists = vim.api.nvim_buf_is_valid(buf_num)
    return listed and exists
end

return M
