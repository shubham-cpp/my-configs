local M = {}

-- Loads this stuff before the plugn is actually loaded
-- Only this way the stuff is applied
function M.setup()
    vim.g.qs_highlight_on_keys = {'f', 'F', 't', 'T'}
    vim.g.qs_buftype_blacklist = {'terminal', 'nofile','fzf','floaterm','NvimTree'}
    vim.g.qs_lazy_highlight = 1

    -- vim.cmd [[ highlight QuickScopePrimary guifg=orange gui=underline ctermfg=155 cterm=underline ]]
    -- vim.cmd [[ highlight QuickScopeSecondary guifg=yellow gui=underline ctermfg=81 cterm=underline ]]

    -- vim.cmd [[ highlight QuickScopePrimary guifg='#00C7DF' gui=underline ctermfg=155 cterm=underline ]]
    -- vim.cmd [[ highlight QuickScopeSecondary guifg='#bd93f9' gui=underline ctermfg=81 cterm=underline ]]
end

return M
