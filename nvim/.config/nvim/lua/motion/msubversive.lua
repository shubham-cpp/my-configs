local M = {}
-- Utility servers
function M.map(type, key, value, opts)
    local opts = opts or {silent = true}
    vim.api.nvim_set_keymap(type, key, value, opts);
end

function M.config()
    M.map("n", "x", "<plug>(SubversiveSubstitute)")
    M.map("n", "xx", "<plug>(SubversiveSubstituteLine)")
    M.map("n", "X", "<plug>(SubversiveSubstituteToEndOfLine)")

    M.map("n", "<Leader>xb", "<plug>(SubversiveSubstituteRange)")
    M.map("x", "<Leader>xb", "<plug>(SubversiveSubstituteRange)")
    M.map("n", "<Leader>xbw", "<plug>(SubversiveSubstituteWordRange)")

    M.map("o", "ie", ":exec \"normal! ggVG\"<cr>",
          {noremap = true, silent = true})
    M.map("o", "iv", ":exec \"normal! HVL\"<cr>",
          {noremap = true, silent = true})

    M.map("n", "<Leader>cx", "<plug>(SubversiveSubstituteRangeConfirm)")
    M.map("x", "<Leader>cx", "<plug>(SubversiveSubstituteRangeConfirm)")
    M.map("n", "<Leader>cxw", "<plug>(SubversiveSubstituteWordRangeConfirm)")
end

return M
