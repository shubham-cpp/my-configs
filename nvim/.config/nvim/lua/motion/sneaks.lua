local M = {}
local helper = require('helper')

function M.config()
    vim.cmd [[ let g:sneak#label = 1 ]]

    -- case insensitive sneak
    vim.cmd [[ let g:sneak#use_ic_scs = 1 ]]

    -- immediately move to the next instance of search, if you move the cursor sneak is back to default behavior
    vim.cmd [[ let g:sneak#s_next = 1 ]]

    -- Cool prompts
    vim.cmd [[ let g:sneak#prompt = '🕵' ]]
    vim.cmd [[ let g:sneak#prompt = '🔎' ]]

    --  remap so I can use , and ; with f and t
    helper.map("", "gS", "<Plug>Sneak_,", {silent = true})
    helper.map("", "gs", "<Plug>Sneak_,;", {silent = true})

    --  Change the colors
    vim.cmd [[ highlight Sneak guifg=black guibg=#00C7DF ctermfg=black ctermbg=cyan ]]
    vim.cmd [[ highlight SneakScope guifg=red guibg=yellow ctermfg=red ctermbg=yellow ]]
end

return M
