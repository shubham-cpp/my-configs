-- vim.cmd [[ packadd! lualine.nvim ]]
local lualine = require('lualine')

-- One of these options
-- wombat,solarized_dark,papercolor_dark,onedark,oceanicnext,nord,material
-- iceberg_dark,dracula,ayu_mirage,ayu_dark,nightfly
lualine.setup {
    options = {
        theme = 'dracula',
        section_separators = {'', ''},
        component_separators = {'', ''},
        icons_enabled = true
    },
    sections = {
        lualine_a = {{'mode', upper = true}},
        lualine_b = {{'branch', icon = ''}},
        lualine_c = {{'filename', file_status = true},{'diagnostics',sources={'nvim_lsp'}},'diff'},
        lualine_x = {'encoding', 'fileformat', 'filetype'},
        lualine_y = {'progress'},
        lualine_z = {'location'}
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = {'filename'},
        lualine_x = {'location'},
        lualine_y = {},
        lualine_z = {}
    },
    extensions = {'fzf','nvim-tree'}
}
