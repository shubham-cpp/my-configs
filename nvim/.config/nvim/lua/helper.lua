local M = {}
local cmd = vim.cmd

function M.split(s, delimiter)
    local result = {};
    for match in (s .. delimiter):gmatch("(.-)" .. delimiter) do
        table.insert(result, match);
    end
    return result;
end

function M.map(type, key, value, opts)
    local opts = opts or {noremap = true, silent = true}
    vim.api.nvim_set_keymap(type, key, value, opts)
end

function M.bmap(type, key, value, opts)
    local opts = opts or {noremap = true, silent = true}
    vim.api.nvim_buf_set_keymap(0, type, key, value, opts)
end

function M.create_augroup(autocmds, name)
    cmd('augroup ' .. name)
    cmd('autocmd!')
    for _, autocmd in ipairs(autocmds) do
        cmd('autocmd ' .. table.concat(autocmd, ' '))
    end
    cmd('augroup END')
end


function M.is_installed(prog)
	if vim.fn.executable(prog) == 1 then
		return true
	end
	return false
end

return M
