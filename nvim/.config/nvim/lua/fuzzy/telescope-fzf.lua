local actions = require('telescope.actions')
local helper = require('helper')
local M = {}
-- Optional way to setup default values
function M.config()
    require('telescope').setup {
        defaults = {
            find_command = {'rg', '--no-heading', '--with-filename', '--line-number', '--column',},
            mappings = {
                i = {["<esc>"] = actions.close, ["<A-CR>"] = actions.select_tab},
                n = {["<A-CR>"] = actions.select_tab}
            },
            prompt_prefix = "🔍",
            initial_mode = "insert",
            -- get_fuzzy_file,get_generic_fuzzy_sorter,get_fzy_sorter,fuzzy_with_index_bias
            -- file_sorter = require'telescope.sorters'.get_fuzzy_file,
            -- file_sorter = require('telescope.sorters').get_fzy_sorter,
            file_ignore_patterns = {
                ".backup", ".swap", ".langservers", ".session", ".undo", ".git",
                "node_modules", "vendor", ".cache", ".vscode-server", "classes",
                ".venv", "%.png", "%.jpeg", "%.jpg", "%.mkv", "%.mp3", "%.mp4"
            },
            -- generic_sorter = require'telescope.sorters'.fuzzy_with_index_bias,
            -- file_previewer = require'telescope.previewers'.cat.new, -- For buffer previewer use `require'telescope.previewers'.vim_buffer_cat.new`
            -- grep_previewer = require'telescope.previewers'.vimgrep.new, -- For buffer previewer use `require'telescope.previewers'.vim_buffer_vimgrep.new`
            -- qflist_previewer = require'telescope.previewers'.qflist.new, -- For buffer previewer use `require'telescope.previewers'.vim_buffer_qflist.new`
            -- extensions = {
            --     fzy_native = {
            --         override_generic_sorter = false,
            --         override_file_sorter = true
            --     }
            -- }
        }
    }
    -- require('telescope').load_extension('fzy_native')

    helper.map("n", ",p",":lua require'telescope.builtin'.find_files{hidden=true,follow=true}<CR>")
    -- helper.map("n", "<C-p>",
    --            ":lua require'telescope.builtin'.find_files{hidden=true,follow=true}<CR>")
    helper.map("n", "??", "<cmd>lua require'telescope.builtin'.live_grep{}<CR>",
               {noremap = true})
    helper.map("n", "\\\\",
               "<cmd>lua require'telescope.builtin'.current_buffer_fuzzy_find{}<CR>",
               {noremap = true})
    helper.map("n", ",c",
               "<cmd>lua require'telescope.builtin'.find_files{ cwd = '~/.config/nvim/' }<CR>",
               {noremap = true})
    helper.map("n", ",d",
               "<cmd>lua require'telescope.builtin'.find_files{ cwd = '~/Documents/dotfiles',hidden=true,follow=true }<CR>",
               {noremap = true})
    helper.map("n", ",z",
               "<cmd>lua require'telescope.builtin'.spell_suggest{}<CR>",
               {noremap = true})

    -- vim.cmd [[ command! -bang Nvim :lua require'telescope.builtin'.find_files({cwd = '~/.config/nvim/'})<CR> ]]
    -- vim.cmd [[ command! -bang DotFiles :lua require'telescope.builtin'.find_files({hidden=true,cwd = '~/Documents/dotfiles/'})<CR> ]]
end
return M
