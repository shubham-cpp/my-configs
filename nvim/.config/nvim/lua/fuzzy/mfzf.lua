local M = {}
local helper = require('helper')

function M.config()

    vim.g.fzf_action = {
        ['alt-enter'] = 'tab split',
        ['ctrl-x'] = 'split',
        ['ctrl-v'] = 'vsplit'
    }

    --- Few Helpful created using FZF {{{

    vim.cmd [[ command! -bang DotFiles call fzf#vim#files('~/Documents/dotfiles/', fzf#vim#with_preview({'options': ['--layout=reverse']}), <bang>0) ]]
    vim.cmd [[ command! -bang Nvim call fzf#vim#files('~/.config/nvim/', fzf#vim#with_preview({'options': ['--layout=reverse']}), <bang>0) ]]
    vim.cmd [[ command! -bang Progs call fzf#vim#files('~/Documents/Programming/', fzf#vim#with_preview({'options': ['--layout=reverse']}), <bang>0) ]]
    vim.cmd [[ command! -nargs=* -bang RG lua require'fuzzy/mfzf'.ripgrep(<q-args>, <bang>0) ]]

    --- }}}

    --- Findings keymaps to use FZF {{{

    helper.map("n", "<C-p>", "<cmd>Files<cr>")
    helper.map("n", ",n", "<cmd>Nvim<cr>")
    helper.map("n", ",c", "<cmd>Nvim<cr>")
    helper.map("n", ",d", "<cmd>DotFiles<cr>")
    helper.map("n", "\\\\", "<cmd>BLines<cr>", {})
    helper.map("n", "??", ":Rg<Space>", {})

    --- }}}

    --- Global variables for FZF {{{

    vim.g.fzf_history_dir = "~/.local/share/fzf-history"

    if vim.fn.executable('uctags') == 1 then
        vim.g.fzf_tags_command =
            "uctags -R --tag-relative --extras=+f --exclude=.git,pkg,nodemodules,venv --languages=-sql<cr><cr>"
    end

    function M.ripgrep(query, fullscreen)
        local command_fmt =
            "rg --column --line-number --no-heading --color=always --smart-case -- %s || true"
        local initial_command = string.format(command_fmt,
                                              vim.fn.shellescape(query))
        local reload_command = string.format(command_fmt, '{q}')
        local spec = {
            options = {
                '--phony', '--query', query, '--bind',
                'change:reload:' .. reload_command
            }
        }
        vim.fn['fzf#vim#grep'](initial_command, 1,
                               vim.fn['fzf#vim#with_preview'](spec), fullscreen)
    end

    --- }}}

end

return M
