local M = {}

function M.config()

    vim.g.startify_session_dir = vim.g.session_directory

    vim.g.startify_lists = {
        {type = 'files', header = {'   Files'}},
        {type = 'dir', header = {'   Current Directory ' .. vim.fn.getcwd()}},
        {type = 'sessions', header = {'   Sessions'}},
        {type = 'bookmarks', header = {'   Bookmarks'}}
    }
    vim.g.startify_bookmarks = {
        {i = '~/.config/nvim/init.vim'}, {b = '~/.config/bspwm/bspwmrc'},
        {s = '~/.config/sxhkd/sxhkdrc'}, {x = '~/.config/X11/Xresources'},
        {z = '~/.config/zsh/.zshrc'}, '~/Documents/Programming/',
        '~/Documents/GitHub/'
    }

end

return M
