local M = {}

-- This function will define general configs about the plugin
-- Like extra languages being added here
function M.config()

    require('kommentary.config').configure_language("rust", {
        single_line_comment_string = "//",
        multi_line_comment_strings = {"/*", "*/"}
    })
    require('kommentary.config').configure_language("haskell", {
        single_line_comment_string = "--",
        multi_line_comment_strings = {"{-", "-}"}
    })
    require('kommentary.config').configure_language("lua", {
        single_line_comment_string = "--",
        prefer_single_line_comments = true
    })
end

return M
