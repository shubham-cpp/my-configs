local M = {}
local helper = require('helper')
local function tree_cb(cb_name)
    return string.format(":lua require'nvim-tree'.on_keypress('%s')<CR>",
                         cb_name)
end

function M.config()
    -- vim.cmd[[ highlight NvimTreeFolderName guifg=darkwhite gui=bold ]]

    helper.map("n", "<M-e>", "<cmd>NvimTreeToggle<cr>")
    helper.map("n", "<C-n>", "<cmd>NvimTreeFindFile<cr>")
    -- helper.map("n", "<C-r>", "<cmd>NvimTreeRefresh<cr>")

    -- vim.g.nvim_tree_side = 'left' -- | right left by default
    -- vim.g.nvim_tree_width = 40 -- 30 by default
    vim.g.nvim_tree_ignore = {
        '.git', 'node_modules', '.cache', '__pycache__', '.vscode', '.steam',
        '.mozilla', '.pki'
    }
    -- vim.g.nvim_tree_auto_open = 1 -- 0 by default, opens the tree when typing `vim $DIR` or `vim`
    vim.g.nvim_tree_auto_close = 1 -- 0 by default, closes the tree when it's the last window
    -- vim.g.nvim_tree_quit_on_open = 1 -- 0 by default, closes the tree when you open a file
    vim.g.nvim_tree_follow = 1 -- 0 by default, this option allows the cursor to be updated when entering a buffer
    vim.g.nvim_tree_indent_markers = 1 -- 0 by default, this option shows indent markers when folders are open
    vim.g.nvim_tree_hide_dotfiles = 1 -- 0 by default, this option hides files and folders starting with a dot `.`
    vim.g.nvim_tree_git_hl = 1 -- 0 by default, will enable file highlight for git attributes (can be used without the icons).
    vim.g.nvim_tree_root_folder_modifier = ':~' -- This is the default. See :help filename-modifiers for more options
    -- vim.g.nvim_tree_tab_open = 1 -- 0 by default, will open the tree when entering a new tab and the tree was previously open
    vim.g.nvim_tree_width_allow_resize = 1 -- 0 by default, will not resize the tree when opening a file
    -- vim.g.nvim_tree_show_icons = {git = 1, folders = 0, files = 0}
    vim.g.nvim_tree_bindings = {
        ["<CR>"] = tree_cb("edit"),
        ["e"] = tree_cb("edit"),
        ["l"] = tree_cb("edit"),
        ["<2-LeftMouse>"] = tree_cb("edit"),
        ["<2-RightMouse>"] = tree_cb("cd"),
        ["<C-]>"] = tree_cb("cd"),
        ["<C-v>"] = tree_cb("vsplit"),
        ["<C-x>"] = tree_cb("split"),
        ["<M-CR>"] = tree_cb("tabnew"),
        ["<C-t>"] = tree_cb("tabnew"),
        ["<BS>"] = tree_cb("close_node"),
        ["h"] = tree_cb("close_node"),
        ["<S-CR>"] = tree_cb("close_node"),
        ["<Tab>"] = tree_cb("preview"),
        ["I"] = tree_cb("toggle_ignored"),
        ["H"] = tree_cb("toggle_dotfiles"),
        ["."] = tree_cb("toggle_dotfiles"),
        ["R"] = tree_cb("refresh"),
        ["a"] = tree_cb("create"),
        ["d"] = tree_cb("remove"),
        ["r"] = tree_cb("rename"),
        ["<C-r>"] = tree_cb("full_rename"),
        ["x"] = tree_cb("cut"),
        ["c"] = tree_cb("copy"),
        ["p"] = tree_cb("paste"),
        ["[c"] = tree_cb("prev_git_item"),
        ["]c"] = tree_cb("next_git_item"),
        ["-"] = tree_cb("dir_up"),
        ["q"] = tree_cb("close")
    }
end

return M
