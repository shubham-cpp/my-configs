local M = {}

function M.config()
    vim.g.floaterm_keymap_toggle = '<F1>'
    vim.g.floaterm_keymap_next = '<Leader>ff'
    vim.g.floaterm_keymap_prev = '<Leader>fb'
    vim.g.floaterm_keymap_new = '<Leader>fn'
    vim.g.floaterm_keymap_kill = '<Leader>fk'
    vim.g.floaterm_gitcommit = 'floaterm'
    vim.g.floaterm_autoinsert = 1
    vim.g.floaterm_width = 0.7
    vim.g.floaterm_height = 0.7
    vim.g.floaterm_wintitle = 0
    vim.g.floaterm_autoclose = 1
    vim.g.floaterm_autohide = 1
    vim.g.floaterm_shell = 'fish'

    if vim.fn.executable('lf') == 1 then
        vim.cmd [[ command! FM FloatermNew lf ]]
    elseif vim.fn.executable('vifm') == 1 then
        vim.cmd [[ command! FM FloatermNew vifmrun ]]
    else
        vim.cmd [[ command! FM FloatermNew ranger ]]
    end
    -- vim.cmd [[ autocmd FileType toggleterm set nonu ]]
    require'helper'.map("n", "<Space>fl", ":FM<cr>", {silent = true})

end

function M.set_nvim_float()
    require"toggleterm".setup {
        size = 20,
        open_mapping = "<F1>",
        shade_filetypes = {},
        shade_terminals = true,
        start_in_insert = true,
        persist_size = true
    }
    vim.cmd [[ autocmd TermEnter term://*toggleterm#* tnoremap <silent><c-]> <C-\><C-n>:exe v:count1 . "ToggleTerm"<CR> ]]
    vim.cmd [[ autocmd FileType toggleterm set nonu ]]
    vim.cmd [[ command! -count=1 Lf  lua require'toggleterm'.exec("lf",<count>, 12) ]]
end

return M

--[[ require"toggleterm".setup {
    size = 20,
    open_mapping = "<F1>",
    shade_filetypes = {},
    shade_terminals = true,
    persist_size = true,
    direction = 'horizontal'
} ]]
-- vim.cmd [[ command! -count=1 Vif  lua require'toggleterm'.exec("vifmrun",<count>, 12) ]]
-- vim.cmd [[ command! -count=1 Lf  lua require'toggleterm'.exec("lf",<count>, 12) ]]
