vim.g.mapleader = " "

vim.o.hidden = true
vim.o.clipboard=vim.o.clipboard .. "unnamedplus"
-- vim.cmd('set clipboard+=unnamedplus')
vim.o.mouse = 'a'

vim.g.backup = false
vim.o.writebackup = false
vim.o.swapfile = false
vim.o.history = 500

vim.o.cmdheight = 2
vim.o.updatetime = 300

vim.o.shortmess = "filnxtToOFc"
vim.wo.signcolumn = "yes"


vim.wo.number = true
vim.wo.relativenumber = true
-- vim.wo.spell = true

vim.cmd("set iskeyword+=-")
vim.cmd("filetype plugin indent on")

vim.o.wildignorecase = true
vim.o.wildmode = "longest,list,full"
vim.o.wildignore = "*.out,*.o,*.pyc,*~,*.class,*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store,*/venv/*,*/__pycache__/*,*.jpg,*.png,*.svg,*.jpeg,*.JPG"

vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.tagcase = "followscs"

vim.o.lazyredraw = true
vim.o.showmatch = true
vim.g.whichwrap = "b,s,<, >,[,]"

vim.o.timeoutlen = 500
vim.o.errorbells = false
vim.o.visualbell = false

vim.wo.foldcolumn = "1"
vim.wo.colorcolumn = "80"
vim.o.scrolloff = 8

vim.bo.expandtab = true
vim.bo.shiftwidth = 4
vim.bo.tabstop = 4

vim.wo.linebreak = true
vim.wo.cursorline = true
-- vim.bo.textwidth = 0
-- vim.bo.wrapmargin = 0
vim.o.columns = 86
vim.o.showbreak = '>> '
vim.wo.numberwidth = 6
vim.wo.list = false

vim.o.splitbelow = true
vim.o.splitright = true

vim.bo.smartindent = true
-- vim.bo.undofile = "on"
vim.cmd("set undofile")

vim.o.tags = vim.o.tags .. ",./.git/tags"

vim.g.session_directory = string.format("%s/session", vim.fn.stdpath('data'))
vim.g.session_autoload = "no"
vim.g.session_autosave = "no"
vim.g.session_command_aliases = 1

-- vim.cmd[[ silent! call repeat#set("\<Plug>MyWonderfulMap", v:count) ]]
-- this-is-to-check
