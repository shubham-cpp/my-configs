local map = require('helper').map
local opts = {noremap = true}

map('n',',w',':w!<cr>',opts)
map('n','jk','<esc>',opts)

map('n','<leader>ct',':!ctags -R --tag-relative --extras=+f --exclude=.git,pkg --languages=-sql<cr><cr>',opts)

map('','<leader><cr>',':noh<cr>',{silent = true})
map('','0','^',{})

map('n', '<M-j>','mz:m+<cr>`z',{})
map("v", "<M-j>",":m'>+<cr>`<my`>mzgv`yo`z",{})
map("n", "<M-k>","mz:m-2<cr>`z",{})
map("v", "<M-k>",":m'<-2<cr>`>my`<mzgv`yo`z",{})
map('n', '<M-Down>','mz:m+<cr>`z',{})
map("v", "<M-Down>",":m'>+<cr>`<my`>mzgv`yo`z",{})
map("n", "<M-Up>","mz:m-2<cr>`z",{})
map("v", "<M-Up>",":m'<-2<cr>`>my`<mzgv`yo`z",{})

map("","<leader>ss", ":setlocal spell!<cr>",{})

map("n","Y","y$",opts)
map("n","<leader>yp",'"ayy"ap',opts)


map("i","<C-Del>","<C-\\><C-O>D",opts)
map("i","<C-D>","<C-\\><C-O>dw",opts)
map("i","<C-u>","<C-\\><C-O>d0",opts)
map("i","<C-z>","<C-\\><C-O>u",opts)

map("i","<C-a>", "<C-\\><C-O>^",opts)
map("i","<C-e>", "<C-\\><C-O>$",opts)
map("c","<C-a>", "<Home>",opts)
map("c","<C-e>", "<End>",opts)

map("v",">",">gv",{})
map("v","<","<gv",{})

map("n","<F3>",":Autoformat<cr>",opts)
map("n","<leader>/","<Plug>kommentary_line_default",{})
map("n","<M-/>",'"ayy"aP:normal gcc<cr>j',opts)

map("x","<leader>/","<Plug>kommentary_line_default",{})
