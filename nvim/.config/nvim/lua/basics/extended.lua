local default_opts = {noremap = true}
local mmap = require('helper').map

vim.cmd [[ iab xdate <C-r>=strftime("%d/%m/%y %H:%M:%S")<cr> ]]
vim.cmd [[ command! -nargs=0 Reload :source ~/.config/nvim/init.vim ]]

if vim.fn.executable("rg") == 1 then
    vim.o.grepprg = "rg --vimgrep --smart-case --hidden --follow"
end

mmap("c", "$p", "!python -u %", default_opts)
mmap("c", "$g", "!gcc % -o %< && ./%<", default_opts)
mmap("c", "$gc", "!gcc % -o %< && ./%<", default_opts)
mmap("c", "$j", "!javac % && java %<", default_opts)
mmap("c", "$x", "!chmod +x %<cr>", default_opts)
mmap("c", "<C-a>", "<Home>")
mmap("c", "<C-e>", "<End>")

mmap("v", "J", ":m '>+1<CR>gv=gv")
mmap("v", "K", ":m '<-2<CR>gv=gv")

mmap("n", "<Leader>tt", ":vnew term://fish<cr>")
vim.cmd [[ command! Q :q! ]]
-- mmap("t", "<C-[>", "<C-\\><C-n>", default_opts)
-- vim.cmd[[ au FileType floaterm tnoremap <C-[> <C-\><C-n> ]]

vim.g.neoterm_autoscroll = 1
vim.o.inccommand = "nosplit"
vim.o.path = vim.o.path .. '**'
-- vim.bo.undofile = true
-- vim.o.undodir = string.format("%s/undo/", vim.fn.stdpath("data"))
vim.g.formatdef_my_lua = '"lua-format"'
vim.g.formatters_lua = {'my_lua'}
vim.g.formatdef_my_html = '"html-beautify -w 100 -I"'
vim.g.formatters_html = {'my_html'}
vim.g.formatdef_my_haskell = '"brittany"'
vim.g.formatters_haskell = {'my_haskell'}
vim.g.formatters_python = {'black'}

vim.g.UltiSnipsExpandTrigger = "<M-cr>"
vim.g.UltiSnipsJumpForwardTrigger = "<c-j>"
vim.g.UltiSnipsJumpBackwardTrigger = "<c-k>"
vim.g.UltiSnipsEditSplit = "vertical"

vim.g.python3_host_prog = "/usr/bin/python"

vim.api.nvim_set_keymap('n', ',a', ':ArgWrap<CR>', default_opts)

-- mmap("n","<A-e>","<cmd>Lexplore<cr>",{noremap=true,silent=true})
-- vim.g.netrw_winsize = -28
-- vim.g.netrw_banner = 0
-- vim.g.netrw_liststyle = 3
-- vim.g.netrw_browse_split = 4
-- vim.cmd('hi netrwDir guifg=#bd93f9')
