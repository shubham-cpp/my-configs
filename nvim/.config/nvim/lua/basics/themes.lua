vim.o.showtabline   = 2
vim.o.showmode      = false
vim.o.termguicolors = true

-- vim.g.gruvbox_italic            = 1
-- vim.g.gruvbox_improved_strings  = 1
-- vim.g.gruvbox_improved_warnings = 1
-- vim.g.gruvbox_contrast_dark     = 'hard'

-- ocean: #1b2b34
-- medium: #282C34
-- deep:#212112
-- darker:#1d1f21
-- vim.g.oceanic_material_transparent_background = 1
-- vim.g.oceanic_material_background      = "deep"
-- vim.g.oceanic_material_allow_bold      = 1
-- vim.g.oceanic_material_allow_italic    = 1
-- vim.g.oceanic_material_allow_underline = 1
-- vim.g.oceanic_material_allow_undercurl = 1
vim.g.one_nvim_transparent_bg = true

-- vim.cmd [[ colorscheme oceanic_material ]]
vim.cmd [[ colorscheme one-nvim ]]

require('theming/my_lualine')
local tabline = require('theming/tabline')
vim.o.tabline = tabline.init()
