--- Snippet to check if Packer is installed or not {{{
local execute = vim.cmd
local fn = vim.fn

local install_path = fn.stdpath('data') .. '/site/pack/packer/opt/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
    execute('!git clone https://github.com/wbthomason/packer.nvim ' ..
                install_path)
    execute 'packadd packer.nvim'
end

--- }}}

vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
    use {'wbthomason/packer.nvim', opt = true}

    --- LSPs {{{
    -- use {
    --     'neoclide/coc.nvim',
    --     branch = 'release',
    --     requires = {{'SirVer/ultisnips'}, {'honza/vim-snippets'}}
    -- }

    use {'euclidianAce/BetterLua.vim', ft = {'lua'}}
    use {
        'neovim/nvim-lspconfig',
        requires = {
            {'RishabhRD/popfix'}, {'RishabhRD/nvim-lsputils'},
            {'ray-x/lsp_signature.nvim'}
        }
    }
    use {
        'hrsh7th/nvim-compe',
        requires = {
            {'SirVer/ultisnips'}, {'honza/vim-snippets'}, {'hrsh7th/vim-vsnip'}
        },
        config = require("lsp/completions").setup()
    }
    use {'mfussenegger/nvim-jdtls'}
    -- use {'fatih/vim-go', run = ':GoUpdateBinaries',ft={'go'}}
    -- use {'tweekmonster/django-plus.vim'}
    --- }}}

    --- Fuzzy Finders {{{
    use {
        'junegunn/fzf.vim',
        requires = {{'junegunn/fzf', run = ':call fzf#install()'}},
        config = require("fuzzy/mfzf").config()
    }

    -- use {'Yggdroot/LeaderF', run = ':LeaderfInstallCExtension' }
    -- use {
    --     'nvim-telescope/telescope.nvim',
    --     config = require('fuzzy/telescope-fzf').config(),
    --     requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
    -- }
    -- use 'nvim-telescope/telescope-fzy-native.nvim'
    ---}}}

    --- Theming and statusline {{{
    -- use {'glepnir/oceanic-material', opt = true}
    use {'Th3Whit3Wolf/one-nvim'}
    -- If you are using Packer
    -- use {'kaicataldo/material.vim', opt = true}
    -- use {'tjdevries/colorbuddy.nvim',opt = true} -- For creating custom colorscheme
    -- use {'morhetz/gruvbox', opt = true}
    -- use {'nekonako/xresources-nvim', opt = true}

    use {
        "hoob3rt/lualine.nvim",
        requires = {'kyazdani42/nvim-web-devicons', opt = true}
    }

    use {
        'mhinz/vim-startify',
        config = function() require("ide/start-screen").config() end
    }
    --- }}}

    --- Misc {{{
    use {
        'norcalli/nvim-colorizer.lua',
        config = function()
            require"colorizer".setup({"*"}, {
                RGB = true,
                RRGGBB = true,
                -- names    = true;         -- "Name" codes like Blue
                RRGGBBAA = true,
                rgb_fn = true,
                hsl_fn = true,
                css = true,
                css_fn = true
            })
            require('helper').map("n", "<F7>", ":ColorizerToggle<cr>",
                                  {noremap = true})
        end,

        cmd = 'ColorizerToggle'
    }

    use {"jiangmiao/auto-pairs"}
    use {'tpope/vim-repeat'}

    --- }}}

    --- Motions {{{
    use {
        'unblevable/quick-scope',
        setup = require'motion/quickscope-sets'.setup()
    }
    use {'tpope/vim-surround'}
    use {'FooSoft/vim-argwrap', cmd = 'ArgWrap'}
    -- use {'justinmk/vim-sneak', config = require("motion/sneaks").config()}
    use {
        'svermeulen/vim-subversive',
        config = require'motion/msubversive'.config()
    }
    use {
        'phaazon/hop.nvim',
        cmd = {'HopChar2', 'HopWord'}
        --[[ config = function()
             local h = require('helper')
             h.map('n', "s", "<cmd>lua require'hop'.hint_words()<cr>",
                   {silent = true})
             h.map('n', "S", "<cmd>HopChar2<cr>", {})
         end
         ]]
    }
    --- }}}

    --- Comment, Format Indent {{{

    -- use {'b3nj5m1n/kommentary', config = require'ide/commenting'.config()}
    use {
        'tpope/vim-commentary',
        config = function()
            vim.api.nvim_set_keymap('n', '<leader>/', ':Commentary<cr>',
                                    {silent = true})
            vim.api.nvim_set_keymap('x', '<leader>/', ':Commentary<cr>',
                                    {silent = true})
        end
    }
    use {'tommcdo/vim-lion'}

    -- use {'Chiel92/vim-autoformat', cmd = 'Autoformat'}
    use {'sbdchd/neoformat', cmd = 'Neoformat'}

    use {
        'glepnir/indent-guides.nvim',
        config = function()
            require('indent_guides').setup(
                {
                    -- put your options in here
                    -- default options
                    indent_levels = 30,
                    indent_guide_size = 1,
                    indent_start_level = 1,
                    indent_space_guides = true,
                    indent_tab_guides = false,
                    indent_soft_pattern = '\\s',
                    exclude_filetypes = {
                        'help', 'dashboard', 'dashpreview', 'LuaTree', 'vista',
                        'sagahover', 'NvimTree', 'fzf', 'floaterm', 'startify',
                        'packer', 'toggleterm'
                    },
                    even_colors = {fg = '#2E323A', bg = '#34383F'},
                    odd_colors = {fg = '#34383F', bg = '#2E323A'}
                })
        end
    }

    --- }}}

    --- Syntax Enhancement {{{
    -- use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
    use {
        'sheerun/vim-polyglot',
        config = require("lsp/lang-settings").config()
        -- ft = {
        --     'sh', 'zsh', 'bash', 'c', 'cpp', 'cmake', 'html', 'markdown',
        --     'java', 'vim', 'tex', 'css', 'lisp', 'go', 'haskell', 'javascript'
        -- }
    }
    use {'baskerville/vim-sxhkdrc'}
    --- }}}

    --- IDE bloat {{{
    use {'voldikss/vim-floaterm', config = require'ide/floatterm'.config()}
    -- use {
    --     'akinsho/nvim-toggleterm.lua',
    --     config = require'ide/floatterm'.set_nvim_float()
    -- }
    use {
        'kyazdani42/nvim-tree.lua',
        requires = {'kyazdani42/nvim-web-devicons', opt = true},
        config = require'ide/file-manager'.config()
    }
    ---}}}

end)
