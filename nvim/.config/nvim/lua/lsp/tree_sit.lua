-- vim.cmd [[packadd nvim-treesitter]]
--[[ vim.wo.foldmethod = "expr"
vim.wo.foldexpr = "nvim_treesitter#foldexpr()"
]] require'nvim-treesitter.configs'.setup {
    ensure_installed = {
        "json", "html", "css", "javascript", "typescript", "lua", "python",
        "java", "c", "cpp", "haskell", "bash"
    }, -- one of "all", "language", or a list of languages
    -- ensure_installed = {"maintained"},
    -- indent = {enable = true},
    highlight = {
        enable = true, -- false will disable the whole extension
        use_languagetree = true
    },
    rainbow = {enable = true},
    refactor = {
        highlight_definitions = {enable = true},
        -- highlight_current_scope = {enable = true},
        smart_rename = {enable = true, keymaps = {smart_rename = "grr"}},
        navigation = {
            enable = true,
            keymaps = {
                goto_definition = "gnd",
                list_definitions = "gnD",
                list_definitions_toc = "gO",
                goto_next_usage = "<C-,>",
                goto_previous_usage = "<C-.>"
            }
        }
    },
    textobjects = {
        select = {
            enable = true,
            keymaps = {
                ['iF'] = {
                    python = '(function_definition) @function',
                    cpp = '(function_definition) @function',
                    c = '(function_definition) @function',
                    java = '(method_declaration) @function'
                },
                -- or you use the queries from supported languages with textobjects.scm
                ['af'] = '@function.outer',
                ['if'] = '@function.inner',
                ['aC'] = '@class.outer',
                ['iC'] = '@class.inner',
                ['ac'] = '@conditional.outer',
                ['ic'] = '@conditional.inner',
                ['ae'] = '@block.outer',
                ['ie'] = '@block.inner',
                ['al'] = '@loop.outer',
                ['il'] = '@loop.inner',
                ['is'] = '@statement.inner',
                ['as'] = '@statement.outer',
                ['ad'] = '@comment.outer',
                ['am'] = '@call.outer',
                ['im'] = '@call.inner'
            }
        },
        move = {
            enable = true,
            goto_next_start = {
                ["]m"] = "@function.outer",
                ["]]"] = "@class.outer"
            },
            goto_next_end = {
                ["]M"] = "@function.outer",
                ["]["] = "@class.outer"
            },
            goto_previous_start = {
                ["[m"] = "@function.outer",
                ["[["] = "@class.outer"
            },
            goto_previous_end = {
                ["[M"] = "@function.outer",
                ["[]"] = "@class.outer"
            }
        }
    }
}
