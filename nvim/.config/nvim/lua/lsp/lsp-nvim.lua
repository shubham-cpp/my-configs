local lsp = require 'lspconfig'
local helper = require('helper')

-- Utility servers {{{

local map = helper.bmap

-- }}}

-- configuring diagnostics {{{
vim.fn.sign_define("LspDiagnosticsSignError",
                   {text = " ", texthl = "LspDiagnosticsError"})
vim.fn.sign_define("LspDiagnosticsSignWarning",
                   {text = " ", texthl = "LspDiagnosticsWarning"})
vim.fn.sign_define("LspDiagnosticsSignInformation",
                   {text = "", texthl = "LspDiagnosticsInformation"})
vim.fn.sign_define("LspDiagnosticsSignHint",
                   {text = " ", texthl = "LspDiagnosticsHint"})

vim.lsp.handlers['textDocument/publishDiagnostics'] =
    vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
        underline = true,
        virtual_text = true,
        signs = true,
        update_in_insert = false
    })

vim.lsp.set_log_level('info')

-- }}}

-- configuring LSP servers

local on_attach_common = function(client, bufnr) --- {{{
    require'lsp_signature'.on_attach()

    local function buf_set_option(...)
        vim.api.nvim_buf_set_option(bufnr, ...)
    end
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

    map('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>')
    map('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>')
    map('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>')
    map('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>')
    map('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>')
    map('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>')
    map('n', '<leader>gt', '<cmd>lua vim.lsp.buf.type_definition()<CR>')
    map('n', '<leader>gw', '<cmd>lua vim.lsp.buf.document_symbol()<CR>')
    map('n', '<leader>gW', '<cmd>lua vim.lsp.buf.workspace_symbol()<CR>')

    -- ACTION mappings
    map('n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>')
    map('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>')

    -- Few language severs support these three {{{
    -- Set some keybinds conditional on server capabilities
    if client.resolved_capabilities.document_formatting then
        map('n', '<Space>=', '<cmd>lua vim.lsp.buf.formatting()<CR>')
    elseif client.resolved_capabilities.document_range_formatting then
        map('n', '<Space>=', '<cmd>lua vim.lsp.buf.formatting()<CR>')
    end
    -- }}}

    map('n', '<leader>ai', '<cmd>lua vim.lsp.buf.incoming_calls()<CR>')
    map('n', '<leader>ao', '<cmd>lua vim.lsp.buf.outgoing_calls()<CR>')

    -- Diagnostics mapping {{{
    map('n', '<leader>ee',
        '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>')
    map('n', '[g', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
    map('n', ']g', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>')
    -- }}}

    -- Set autocommands conditional on server_capabilities {{{
    if client.resolved_capabilities.document_highlight then
        vim.api.nvim_exec([[
            hi LspReferenceWrite cterm=bold ctermfg=red gui=bold guisp= guifg= guibg=#565575
            hi LspReferenceRead cterm=bold ctermfg=red gui=bold guisp= guifg= guibg=#565575
            hi LspReferenceText cterm=bold ctermfg=red gui=bold guisp= guifg= guibg=#565575
            hi LspDiagnosticsDefaultError cterm=bold ctermbg=red guifg=#ff3333
            hi LspDiagnosticsDefaultWarning cterm=bold ctermbg=red guifg=#e7ae05
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]], false)
    end --- }}}

    print("LSP attached.");
end --- }}}

--- Server Settings {{{

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local clangd_capabilities = capabilities;
clangd_capabilities.textDocument.semanticHighlighting = true;

lsp.clangd.setup({
    capabilities = clangd_capabilities,
    default_config = {
        cmd = {
            "clangd", "--background-index", "--pch-storage=memory",
            "--clang-tidy", "--suggest-missing-includes", "--cross-file-rename",
            "--completion-style=detailed"
        },
        filetypes = {"c", "cpp", "objc", "objcpp"},
        init_options = {
            clangdFileStatus = true,
            usePlaceholders = true,
            completeUnimported = true,
            semanticHighlighting = true
        },
        root_dir = lsp.util.root_pattern("compile_commands.json",
                                         "compile_flags.txt", ".git")
    },
    on_attach = on_attach_common
})

local sumneko_root_path = os.getenv("HOME") ..
                              "/Downloads/GitClones/lua-language-server"
local sumneko_binary = sumneko_root_path .. "/bin/Linux/lua-language-server"
lsp.sumneko_lua.setup({
    cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"},
    capabilities = capabilities,
    settings = {
        Lua = {
            runtime = {version = 'LuaJIT', path = vim.split(package.path, ';')},
            completion = {enable = true, callSnippet = "Both"},
            diagnostics = {
                enable = true,
                globals = {'vim', 'describe'},
                disable = {"lowercase-global"}
            },
            workspace = {
                library = {
                    [vim.fn.expand('$VIMRUNTIME/lua')] = true,
                    [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
                    [vim.fn.expand('/usr/share/awesome/lib')] = true
                },
                maxPreload = 2000,
                preloadFileSize = 1000
            }
        }
    },
    on_attach = on_attach_common
})

local nvim_lsp = require 'lspconfig'
local configs = require 'lspconfig/configs'
configs.emmet_ls = {
    default_config = {
        cmd = {'emmet-ls', '--stdio'},
        filetypes = {'html', 'css'},
        root_dir = function() return vim.loop.cwd() end,
        settings = {}
    }
}
nvim_lsp.emmet_ls.setup {
    capabilities = capabilities,
    on_attach = on_attach_common
}

local servers = {
    "pyright", "vimls", "yamlls", "jsonls", "html", "cssls", "bashls", "texlab",
    "hls", "tsserver"
}

for _, lang in ipairs(servers) do
    lsp[lang].setup {capabilities = capabilities, on_attach = on_attach_common}
end

lsp.gopls.setup({
    cmd = {"gopls", "serve"},
    -- cmd = {'gopls', '--remote=auto'},
    settings = {gopls = {analyses = {unusedparams = true}, staticcheck = true}},
    root_dir = lsp.util.root_pattern(".git","go.mod","."),
    capabilties = capabilities,
    init_options = {usePlaceholders = true, completeUnimported = true},
    on_attach = on_attach_common
})

-- }}}

-- nvim-lsputils configuration {{{
-- local actions = require 'lsputil.actions'
vim.g.lsp_utils_location_opts = {
    height = 24,
    mode = 'split',
    list = {border = true, numbering = true},
    preview = {title = 'Location Preview', border = true}
}

vim.g.lsp_utils_symbols_opts = {
    height = 24,
    mode = 'editor',
    list = {border = true, numbering = false},
    preview = {title = 'Symbols Preview', border = true},
    prompt = {}
}
vim.lsp.handlers['textDocument/codeAction'] =
    require'lsputil.codeAction'.code_action_handler
vim.lsp.handlers['textDocument/references'] =
    require'lsputil.locations'.references_handler
vim.lsp.handlers['textDocument/definition'] =
    require'lsputil.locations'.definition_handler
vim.lsp.handlers['textDocument/declaration'] =
    require'lsputil.locations'.declaration_handler
vim.lsp.handlers['textDocument/typeDefinition'] =
    require'lsputil.locations'.typeDefinition_handler
vim.lsp.handlers['textDocument/implementation'] =
    require'lsputil.locations'.implementation_handler
vim.lsp.handlers['textDocument/documentSymbol'] =
    require'lsputil.symbols'.document_handler
vim.lsp.handlers['workspace/symbol'] =
    require'lsputil.symbols'.workspace_handler

--- }}}

require("lsp/formatting-lsp")
