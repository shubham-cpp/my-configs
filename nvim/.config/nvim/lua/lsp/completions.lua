local M = {}
local remap = require('helper').map

function M.setup()
    vim.o.completeopt = "menuone,noselect"
    vim.lsp.protocol.CompletionItemKind =
        {
            "ﮜ [text]", " [method]", " [function]", " [constructor]",
            "ﰠ [field]", " [variable]", " [class]", " [interface]",
            " [module]", " [property]", " [unit]", " [value]",
            " [enum]", " [key]", " [Snippet]", " [color]",
            " [file]", " [reference]", " [folder]",
            " [enum member]", " [constant]", " [struct]",
            "⌘ [event]", " [operator]", "⌂ [type]"
        }

    require'compe'.setup {
        -- enabled = true,
        -- autocomplete = true,
        -- min_length = 1,
        -- preselect = 'enable',
        -- throttle_time = 80,
        -- source_timeout = 150,
        -- incomplete_delay = 300,
        -- max_abbr_width = 80,
        -- max_kind_width = 80,
        -- max_menu_width = 80,
        documentation = true,

        source = {
            ultisnips = true,
            buffer = true,
            path = true,
            calc = true,
            vsnip = true,
            nvim_lsp = true,
            nvim_lua = true,
            spell = true,
            tags = true
            -- omni = true
            -- snippets_nvim = true
        }
    }

    local t = function(str)
        return vim.api.nvim_replace_termcodes(str, true, true, true)
    end
    local check_back_space = function()
        local col = vim.fn.col('.') - 1
        if col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
            return true
        else
            return false
        end
    end
    -- -- Use (s-)tab to:
    -- -- move to prev/next item in completion menuone
    -- -- jump to prev/next snippet's placeholder
    _G.tab_complete = function()
        if vim.fn.pumvisible() == 1 then
            return t "<C-n>"
        elseif vim.fn.call("vsnip#available", {1}) == 1 then
            return t "<Plug>(vsnip-expand-or-jump)"
        elseif check_back_space() then
            return t "<Tab>"
        else
            return vim.fn['compe#complete']()
        end
    end
    _G.s_tab_complete = function()
        if vim.fn.pumvisible() == 1 then
            return t "<C-p>"
        elseif vim.fn.call("vsnip#jumpable", {-1}) == 1 then
            return t "<Plug>(vsnip-jump-prev)"
        else
            return t "<S-Tab>"
        end
    end

    vim.api.nvim_set_keymap("i", "<Tab>", "v:lua.tab_complete()", {expr = true})
    vim.api.nvim_set_keymap("s", "<Tab>", "v:lua.tab_complete()", {expr = true})
    vim.api.nvim_set_keymap("i", "<S-Tab>", "v:lua.s_tab_complete()",
                            {expr = true})
    vim.api.nvim_set_keymap("s", "<S-Tab>", "v:lua.s_tab_complete()",
                            {expr = true})
    remap("i", "<C-Space>", "compe#complete()", {silent = true, expr = true})

    remap('i', '<CR>', "compe#confirm('<CR>')", {expr = true, noremap = true})
    -- remap('i', '<Tab>', 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', {expr = true})
    -- remap('i', '<S-Tab>', 'pumvisible() ? "\\<C-p>" : "\\<Tab>"', {expr = true})
end

return M
