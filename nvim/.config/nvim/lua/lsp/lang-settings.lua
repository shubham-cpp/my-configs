-- disable vim-go :GoDef short cut (gd)
--  this is handled by LanguageClient [LC]
local M = {}

function M.config()

    vim.g.go_def_mapping_enabled = 0
    --  Go syntax highlighting
    vim.g.go_highlight_fields = 1
    vim.g.go_highlight_functions = 1
    vim.g.go_highlight_function_calls = 1
    vim.g.go_highlight_extra_types = 1
    vim.g.go_highlight_operators = 1

    --  Auto formatting and importing
    vim.g.go_fmt_autosave = 1
    vim.g.go_fmt_command = "goimports"

    --  For C/C++
    vim.g.cpp_attributes_highlight = 1
    vim.g.cpp_class_scope_highlight = 1
    vim.g.cpp_class_decl_highlight = 1
    vim.g.cpp_concepts_highlight = 1
    vim.g.c_curly_error = 1
    vim.g.c_comment_strings = 1
    vim.g.cpp_experimental_simple_template_highlight = 1
    vim.g.cpp_member_variable_highlight = 1
    vim.g.cpp_posix_standard = 1
    vim.g.cpp_simple_highlight = 1
    --  let c_no_curly_error = 1
    -- For python
    vim.g.python_highlight_all = 1
    -- au FileType python :Python3Syntax<cr>
    -- For haskell
    vim.g.hs_highlight_boolean = 1
    vim.g.hs_allow_hash_operator = 1
    vim.g.haskell_enable_quantification = 1 -- to enable highlighting of `forall`
    vim.g.haskell_enable_recursivedo = 1 -- to enable highlighting of `mdo` and `rec`
    vim.g.haskell_enable_arrowsyntax = 1 -- to enable highlighting of `proc`
    vim.g.haskell_enable_pattern_synonyms = 1 -- to enable highlighting of `pattern`
    vim.g.haskell_enable_typeroles = 1 -- to enable highlighting of type roles
    vim.g.haskell_enable_static_pointers = 1 -- to enable highlighting of `static`
    vim.g.haskell_backpack = 1 -- to enable highlighting of backpack keywords

end

return M
