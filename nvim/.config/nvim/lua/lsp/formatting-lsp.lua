require"lspconfig".efm.setup {
    cmd = {"efm-langserver"},
    init_options = {documentFormatting = true, codeAction = false}
}
require'helper'.map('n', '<F3>', ':lua vim.lsp.buf.formatting()<cr>')
print("EFM Started")
