" Set up persistent undo across all files.
" set undofile
lua require("packer-plugins")

lua require("basics/basic")
lua require("basics/extended")
lua require("basics/themes")

lua require("lsp/lsp-nvim")
" source ~/.config/nvim/viml/mcoc.vim
source ~/.config/nvim/viml/auto_commands.vim
source ~/.config/nvim/viml/keys.vim

" Setting the tabline because lua doesnt do it
function! TabLine()
	return luaeval("require'theming.tabline'.init()")
endfunction

set tabline=%!TabLine()
