lua require('lsp/lsp-nvim')
set omnifunc=v:lua.vim.lsp.omnifunc
set completeopt=menuone,noinsert,noselect
" set completeopt+=menu,longest,menuone,noinsert,noselect
set shortmess+=c

" Fix for autopairs to behave as expected
let g:completion_confirm_key = ""
imap <expr> <cr>  pumvisible() ? complete_info()["selected"] != "-1" ?
            \ "\<Plug>(completion_confirm_completion)"  :
            \ "\<c-e>\<CR>" : "\<CR>"

let g:diagnostic_enable_virtual_text = 1

let g:completion_enable_snippet = "UltiSnips"
let g:completion_enable_auto_paren = 1
let g:completion_matching_smart_case = 1
let g:completion_enable_auto_popup = 1
let g:enable_auto_signature = 1
let g:completion_auto_change_source = 1
let g:completion_trigger_keyword_length = 1 " default = 1
let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy', 'all']
let g:completion_sorting = 'strategy'
let g:completion_chain_complete_list = [
            \{'complete_items': ['lsp', 'snippet']},
            \{'complete_items': ['path'], 'triggered_only': ['/']},
            \{'mode': '<c-p>'},
            \{'mode': '<c-n>'},
            \]

imap <silent> <c-space> <Plug>(completion_trigger)
imap <tab> <Plug>(completion_smart_tab)
imap <s-tab> <Plug>(completion_smart_s_tab)
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
" Use completion-nvim in every buffer
augroup CompletionTriggerCharacter
    autocmd!
    autocmd BufEnter * lua require'completion'.on_attach()
    autocmd BufEnter * let g:completion_trigger_character = ['.']
    autocmd BufEnter *.c,*.cpp let g:completion_trigger_character = ['.', '::']
augroup end
