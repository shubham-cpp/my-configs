augroup Highlight_Yank " {{{
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank()
    au FileType vim,lua set foldmethod=marker
    au FileType vim nnoremap <buffer> <silent> <F5> :so %<cr>
    au FileType vim,tex let b:autoformat_autoindent=0
    au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
    au TermOpen term://* startinsert
augroup END " }}}

augroup Dynamic_Smartcase " {{{
    autocmd!
    au CmdLineEnter : set nosmartcase
    au CmdLineLeave : set smartcase
augroup END "}}}

" For Filehandling {{{
augroup File_Reloads
    autocmd!
    au BufWritePost Xresources,Xdefaults !xrdb -load -merge %
    au BufEnter *profile set filetype=sh
    au BufWritePost profile,dir_icons !source ~/.config/X11/profile
    au BufEnter lfrc set filetype=fish
    au BufEnter vifmrc set filetype=vim
    " Automatically deletes all trailing whitespace and newlines at end of file on save.
    au BufWritePre * %s/\s\+$//e
    au BufWritepre * %s/\n\+\%$//e
    au FocusGained,BufEnter * checktime
    au FileType java lua require("lsp/jdt_setup").setup()
augroup end " }}}

augroup Comments " {{{
    autocmd!
    au FileType apache,sxhkdrc setlocal commentstring=#\ %s
    au FileType htmldjango setlocal commentstring=<!--%s-->
    au BufEnter Xresources setlocal commentstring=!\ %s
    au BufEnter template setlocal commentstring=#\ %s
    au FileType * set formatoptions-=c formatoptions-=r formatoptions-=o
    au FileType c,cpp setlocal comments-=:// comments+=f://
augroup end " }}}

augroup Plugin_Confs "{{{
    autocmd!
    au FileType vim,json let g:argwrap_line_prefix='\'
    au FileType lua let g:argwrap_line_prefix=' '
    au VimEnter * silent exec "!kill -s SIGWINCH $PPID"
    au ColorScheme oceanic_material hi QuickScopePrimary guifg=orange gui=underline ctermfg=155 cterm=underline
    au ColorScheme oceanic_material hi QuickScopeSecondary guifg=yellow gui=underline ctermfg=81 cterm=underline
    au ColorScheme oceanic_material hi NvimTreeFolderName guifg=darkwhite gui=bold
    au BufWritePost packer-plugins.lua PackerCompile

    au ColorScheme challenger_deep hi Folded ctermfg=white ctermbg=Grey guifg=white guibg=#100e23
    au colorscheme challenger_deep hi ColorColumn ctermbg=white guibg=#313131

    " au colorscheme * hi MatchParen guibg=#fe7346 guifg=#0b2940
    " au ColorScheme * hi TelescopeSelection guifg='#bd93f9' gui=underline ctermfg=81 cterm=underline
    " au ColorScheme * hi TelescopeMatching guifg=Orange gui=underline ctermfg=155 cterm=underline
    " autocmd TabNew,TabLeave,TabClosed,TabEnter * set tabline=%!TabLine()
augroup END "}}}

augroup Highlights " {{{
    autocmd!
    "     autocmd BufEnter * hi TabLineSel gui=Bold guibg=#bd93f9 guifg=#292929
    "     autocmd BufEnter * hi TabLineSelSeparator gui=bold guifg=#bd93f9
    "     autocmd BufEnter * hi TabLine guibg=#504945 guifg=#b8b894 gui=None
    "     autocmd BufEnter * hi TabLineSeparator guifg=#504945
    "     autocmd BufEnter * hi TabLineFill guibg=None gui=None
    au FileType netrw hi netrwLink guifg=#0095ff
    au FileType netrw hi netrwDir guifg=#bd93f9
    au FileType netrw hi netrwSymLink guifg=#0095ff
augroup END " }}}
