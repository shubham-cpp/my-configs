nnoremap ,w :w!<cr>
inoremap jk <esc>
" inoremap <esc> <nop>

" Auto generate tags using <leader>ct
if executable('ctags')
    nnoremap <leader>ct :!ctags -R --tag-relative --extras=+f --exclude=.git,pkg --languages=-sql<cr><cr>
endif
if executable('uctags')
    nnoremap <leader>ct :!uctags -R --tag-relative --extras=+f --exclude=.git,pkg --languages=-sql<cr><cr>
endif

nmap <silent> <Esc> :noh<cr>
map 0 ^

" Move a line of text using ALT+[jk] --------- {{{
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z
nmap <M-Down> mz:m+<cr>`z
nmap <M-Up> mz:m-2<cr>`z
vmap <M-Down> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-Up> :m'<-2<cr>`>my`<mzgv`yo`z
" }}}
"
" Handling the spelling inside vim ----------------{{{
map <leader>ss :setlocal spell!<cr>
" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
" }}}

" Copy the Line from the cursor to end of line just like D
nnoremap Y y$
nnoremap <Leader>yp "ayy"ap

" Using the function of D (of normal mode) in insert mode {{{
inoremap <C-Del> <C-\><C-O>D
inoremap <C-D> <C-\><C-O>dw
inoremap <C-u> <C-\><C-O>d0
inoremap <C-z> <C-\><C-O>u
" }}}

" To delete the next word under the cursor
inoremap <C-a> <C-\><C-O>^
inoremap <C-e> <C-\><C-O>$
cnoremap <C-a> <Home>
cnoremap <C-e> <End>

" Keeps the current visual block selection active after changing indent
vmap > >gv
vmap < <gv

" Smart way to move between windows
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l
" Smart way to nabigate if using terminal window
tnoremap <C-h> <C-\\><C-N><C-w>h
tnoremap <C-j> <C-\\><C-N><C-w>j
tnoremap <C-k> <C-\\><C-N><C-w>k
tnoremap <C-l> <C-\\><C-N><C-w>l

nnoremap <S-Left>   :tabp<cr>
nnoremap <S-Right>  :tabn<cr>
nnoremap <M-Left>   :-tabmove<cr>
nnoremap <M-Right>  :+tabmove<cr>

map <leader>cd <cmd>cd %:p:h<cr>:pwd<cr>
nnoremap <leader>v v$
 "To resize vertical splits
nnoremap <silent> <M-=> :exe "vertical resize +10"<CR>
nnoremap <silent> <M--> :exe "vertical resize -10"<CR>
nnoremap <Leader>e      :edit <C-r>=expand("%:p:h")<cr>/
nnoremap <Leader>te     :tabedit <C-r>=expand("%:p:h")<cr>/
nnoremap <Leader>tv     :vnew <C-r>=expand("%:p:h")<cr>/

" For handling Search and replace
nnoremap <silent> ,s :let @/='\<'.expand('<cword>').'\>'<CR>cgn
xnoremap <silent> ,s "sy:let @/=@s<CR>cgn

" nnoremap <silent> ,a :ArgWrap<CR>
" nnoremap <F3> :Autoformat<cr>
" nnoremap <F3> :Neoformat<cr>
nnoremap <F7> :ColorizerToggle<cr>

" Commenting {{{
" nmap <silent> <leader>/ <Plug>kommentary_line_default
" map <silent> <leader>/ :Commentary<cr>
" xmap <silent> <leader>/ :Commentary<cr>
" xmap <leader>/ <Plug>kommentary_visual_default
nnoremap <silent> <M-/> "ayy"aP:normal gcc<cr>j
" }}}

" Hop char motion {{{
nmap <silent> s :HopWord<cr>
nmap S :HopChar2<cr>
" }}}
