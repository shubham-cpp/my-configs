call plug#begin(stdpath('data').'/plugged')

" Adds fuzzy finding in nvim ----------------- {{{
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } | Plug 'junegunn/fzf.vim'

" Meant to be better fzf(written in Complete Lua)
Plug 'nvim-lua/popup.nvim' | Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-lua/telescope.nvim'

" }}}

" Lsp support for neovim. Coc is equivalent to vscode's lsp {{{

Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'RishabhRD/popfix'
Plug 'RishabhRD/nvim-lsputils'
Plug 'tjdevries/nlua.nvim'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
Plug 'tweekmonster/django-plus.vim'

" }}}

" Icons Statusline and Colors {{{

Plug 'mhinz/vim-startify'
Plug 'kyazdani42/nvim-web-devicons'
" Plug 'ryanoasis/vim-devicons'
" Plug 'hardcoreplayers/spaceline.vim'
" Plug 'glepnir/galaxyline.nvim'
Plug 'itchyny/lightline.vim'
Plug 'norcalli/nvim-colorizer.lua'

" }}}

" Commenting Plugin 		" Making words,lines,etc wrap around brackets,quotes,etc better {{{

Plug 'jiangmiao/auto-pairs'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'FooSoft/vim-argwrap'
Plug 'unblevable/quick-scope'

" }}}

" Themes   ------------------------ {{{

Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'drewtempelmeyer/palenight.vim', {'on':'palenight'}
Plug 'morhetz/gruvbox', {'on':'gruvbox'}
Plug 'mhartington/oceanic-next', {'on':'OceanicNext'}
Plug 'ghifarit53/tokyonight-vim', {'on':'tokyonight'}
" Plug 'glepnir/zephyr-nvim', {'on':'zephyr'}
Plug 'glepnir/oceanic-material'
" }}}

" Easy motion plugin -------------------- {{{
Plug 'justinmk/vim-sneak'
" Plug 'easymotion/vim-easymotion'
"     Plug 'haya14busa/incsearch.vim'
"     Plug 'haya14busa/incsearch-easymotion.vim'
"     Plug 'haya14busa/incsearch-fuzzy.vim'

" }}}

" Plugins that does the donkey work {{{

Plug 'svermeulen/vim-subversive'
Plug 'Chiel92/vim-autoformat'
" Plug 'airblade/vim-gitgutter'
Plug 'tommcdo/vim-lion'
Plug 'kkoomen/vim-doge', { 'do': { -> doge#install() } }

" }}}

" Better Synatx Highlighting in Vim {{{
" Plug 'nvim-treesitter/nvim-treesitter'
Plug 'sheerun/vim-polyglot'
" " Additional Polyglot Setting for various filetypes
Plug 'bfrg/vim-cpp-modern'
Plug 'tbastos/vim-lua'
Plug 'vim-python/python-syntax'
Plug 'pboettch/vim-cmake-syntax'
Plug 'georgewitteman/vim-fish'
Plug 'arzg/vim-sh'
Plug 'sheerun/html5.vim'
Plug 'gutenye/json5.vim'
Plug 'baskerville/vim-sxhkdrc'
Plug 'euclidianAce/BetterLua.vim'

" }}}

" Few additional IDE features that it Love {{{

Plug 'voldikss/vim-floaterm'
Plug 'preservim/tagbar',{'on': 'TagbarToggle'}
Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': ':UpdateRemotePlugins'}
Plug 'mbbill/undotree', {'cmd': 'UndotreeToggle'}

" }}}

call plug#end()
