#!/bin/bash

run() {
    if ! pgrep $1 ; then
        $@ &
    fi
}

# bash ~/.local/bin/myscripts/default_mouse_settings
"$HOME"/.config/polybar/launch.sh &
# bash ~/.local/bin/myscripts/if_void &

killall sxhkd
sxhkd &

# run lxqt-policykit-agent
run dunst
# run nitrogen --restore
# run xcompmgr -c -C -t-5 -l-5 -r4.2 -o.55
# run picom --experimental-backends
# run feh --no-fehbg --bg-fill ~/.config/wall.jpg
# run udiskie -aNt -f pcmanfm-qt
# run kitty
# run redshift-gtk
# run redshift
# run greenclip daemon
