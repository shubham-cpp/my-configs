(setq doom-font (font-spec :family "JetBrainsMono Nerd Font" :size 13)
      doom-variable-pitch-font (font-spec :family "RobotoMono Nerd Font" :size 13)
      doom-big-font (font-spec :family "JetBrainsMono Nerd Font" :size 21))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))
(setq global-prettify-symbols-mode t)

(setq doom-theme 'doom-challenger-deep)

(setq display-line-numbers-type 'relative)

(add-hook! 'before-save-hook 'delete-trailing-lines)
(add-hook! 'before-save-hook 'delete-trailing-whitespace)

(add-hook 'after-change-major-mode-hook
          (lambda ()
            (modify-syntax-entry ?_ "w")
            (modify-syntax-entry ?- "w")))

(setq user-full-name "Shubham Pawar"
      user-mail-address "shubhampawar3007@gmail.com")

(setq company-minimum-prefix-length 1
      company-idle-delay 0.2
      evil-snipe-scope 'buffer)

(defun my/save-and-kill-this-buffer ()
    (interactive)
    (save-buffer)
    (kill-this-buffer))

(evil-ex-define-cmd "q" #'kill-this-buffer)
(evil-ex-define-cmd "wq" #'my/save-and-kill-this-buffer)

(with-eval-after-load 'dired
  (setq delete-by-moving-to-trash t))

(setq maximum-scroll-margin 0.5
      scroll-margin 50
      scroll-preserve-screen-position t
      scroll-conservatively 0)

(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu4e")
;;(require 'smtpmail)
(setq mu4e-get-mail-command "mbsync -c ~/.config/isync/mbsyncrc -a"
      mu4e-update-interval  300
      mu4e-main-buffer-hide-personal-addresses t
      message-send-mail-function 'smtpmail-send-it
      starttls-use-gnutls t
      smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
      mu4e-sent-folder "/clg-mail/Sent"
      mu4e-drafts-folder "/clg-mail/Drafts"
      mu4e-trash-folder "/clg-mail/Trash"
      mu4e-maildir-shortcuts
      '(("/account-1/Inbox"      . ?i)
        ("/account-1/Sent Items" . ?s)
        ("/account-1/Drafts"     . ?d)
        ("/account-1/Trash"      . ?t)))

(setq org-directory "~/Documents/org/")

(with-eval-after-load 'org
  ;; This is needed as of Org 9.2
  (require 'org-tempo)

  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python")))

(with-eval-after-load 'org
    (setq evil-replace-with-register-key (kbd "x"))
    (evil-replace-with-register-install)
    (evil-global-set-key 'normal (kbd "X") (kbd "x $")))

(map! (:map emacs-lisp-mode-map
       :leader
       :desc "Eval" "e" (:ignore t :which-key "Eval")
       :desc "Buffer" "eb" #'eval-buffer
       :desc "Current Line" "ee" #'eros-eval-last-sexp
       :desc "Function" "ed" #'eros-eval-defun))

(evil-define-key nil evil-normal-state-map
  "0" 'evil-first-non-blank
  (kbd "<escape>") 'evil-ex-nohighlight
  ",w" 'save-buffer
  "j" 'evil-next-visual-line
  "k" 'evil-previous-visual-line)

(evil-global-set-key 'insert (kbd "C-S-v") #'yank)

(map! :leader
      :desc "Toggle Comment Line" "/" #'comment-line
      :desc "Search Projec" ";" '+default/search-project
      :desc "Flycheck" "t F" #'flycheck-mode
      :desc "Fullscreen Mode" "t f" #'toggle-frame-fullscreen
      :desc "balance-windows " "w +" #'balance-windows
      :desc "evil-window-decrease-height" "w -" #'(lambda ()
                                                    (interactive)
                                                    (evil-window-decrease-width 8))
      :desc "evil-window-increase-height" "w =" #'(lambda ()
                                                    (interactive)
                                                    (evil-window-increase-width 8))
      :desc "Open Private Configs" "f p" #'(lambda ()
                                             (interactive)
                                             (counsel-find-file "~/.config/doom")))

(use-package! uniquify
  :config
  (setq uniquify-buffer-name-style 'reverse))

(use-package! saveplace
  :config
  (setq save-place-file "~/.cache/saveplace")
  (setq-default save-place t))

(use-package! dired-single
  :commands (dired dired-jump))

(use-package evil-replace-with-register
  :config
    (setq evil-replace-with-register-key (kbd "x"))
    (evil-replace-with-register-install)
    (evil-global-set-key 'normal (kbd "X") (kbd "x $")))
